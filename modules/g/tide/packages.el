;; -*- no-byte-compile: t; -*-
;;; g/tide/packages.el
(package! tide)
(package! typescript-mode)
(package! eslintd-fix)
