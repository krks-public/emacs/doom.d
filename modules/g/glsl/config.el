;;; g/glsl/config.el -*- lexical-binding: t; -*-


;; (defun graphql-init()
;;   (message "initializing graphql")
;;   (doom|enable-line-numbers))

(def-package! glsl-mode
  :mode "\\.glsl$")
