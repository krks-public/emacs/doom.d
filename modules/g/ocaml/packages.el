;; -*- no-byte-compile: t; -*-
;;; g/ocaml/packages.el


(package! merlin)
(package! lsp-ocaml)
(package! reason-mode :recipe (:fetcher github :repo "reasonml-editor/reason-mode"))
(package! tuareg)
