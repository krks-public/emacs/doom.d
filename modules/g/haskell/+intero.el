;;; g/haskell/+intero.el -*- lexical-binding: t; -*-
;;;###if (featurep! +intero)

(def-package! intero
  :commands intero-mode
  :init
  (setq intero-extra-ghc-options '("-Wall -fno-warn-type-defaults -Werror=incomplete-patterns"))
  (defun +haskell|init-intero ()
    "Initializes `intero-mode' in haskell-mode, unless stack isn't installed.
This is necessary because `intero-mode' doesn't do its own error checks."
    (when (derived-mode-p 'haskell-mode)
      (if (executable-find "stack")
          (intero-mode +1)
        (message "Couldn't find stack. Refusing to enable intero-mode."))))
  (add-hook 'haskell-mode-hook #'+haskell|init-intero)
  :config
  (add-hook 'intero-mode-hook #'flycheck-mode)
  (add-hook 'intero-mode-hook
            '(lambda () (flycheck-add-next-checker 'intero
                                              '(warning . haskell-hlint))))
  (set-lookup-handlers! 'haskell-mode :definition #'intero-goto-definition))


(def-package! hindent
  :hook (haskell-mode . hindent-mode))
